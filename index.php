<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$myObj = new Animal("shaun");

echo "Name : " . $myObj->name . "<br>";
echo "Legs : " . $myObj->legs . "<br>";
echo "cold blooded : " . $myObj->cold_blooded . "<br>";

echo "<br>";

$myObj1 = new Ape("buduk");
echo "Name : " . $myObj1->name . "<br>";
echo "Legs : " . $myObj1->legs . "<br>";
echo "cold blooded : " . $myObj1->cold_blooded . "<br>";
$myObj1->Jump();

echo "<br><br>";

$myObj2 = new Frog("kera sakti");
echo "Name : " . $myObj2->name . "<br>";
echo "Legs : " . $myObj2->legs . "<br>";
echo "cold blooded : " . $myObj2->cold_blooded . "<br>";
$myObj2->Yell();
