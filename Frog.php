<?php

require_once('Animal.php');

class Frog extends Animal
{
    public $name;
    public $legs = 2;

    public function Yell()
    {
        echo "Yell : Auooo";
    }
}
